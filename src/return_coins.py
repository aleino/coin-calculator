
import os
import sys

COFFEE_PRICE = os.environ.get('COFFEE_PRICE', 314)

VALID_BILLS = [
    50000,
    20000,
    10000,
    5000,
    2000,
    1000,
    500
]

VALID_COINS = [
    200,
    100,
    50,
    20,
    10,
    5
]


def validate_eur_inserted(eur_inserted):
    if eur_inserted <= 0:
        raise ValueError('Inserted amount must be positive.')


def return_coins(coffee_price, cents_inserted):
    """Calculate amount of coins to return to the customer.
    The coin shoe is assumed to be infite.

    Args:
        coffee_price    (int)  Coffee price in cents.
        cents_inserted  (int)  Coffee price in cents.

    Returns:
        list of (int, int)      List of coin values to
                                return (nominal_value, amount).
    """
    validate_eur_inserted(cents_inserted)
    cents_to_return = cents_inserted - coffee_price
    coins_to_return = []

    cents_remaining = cents_to_return
    for nominal_value in VALID_COINS:
        if nominal_value > cents_remaining:
            continue
        n_coins, cents_remaining = divmod(cents_remaining, nominal_value)
        coins_to_return.append((nominal_value, n_coins))

    return coins_to_return


def print_result(coins_to_return):
    print('Thank You for your purchase! Your change is:')
    print('Number of coins, coin value')
    for nominal_value, amount in coins_to_return:
        print('{:4d} {:.2f}'.format(amount, nominal_value / 100))


if __name__ == "__main__":
    try:
        cents_inserted = int(sys.argv[1])
    except IndexError as ex:
        print('Usage: python calculate_change.py <EUR_CENTS_INSERTED>')
        sys.exit()

    returned_coins = return_coins(COFFEE_PRICE, cents_inserted)
    print_result(returned_coins)
