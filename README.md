# Coin Change Calculator

Calculates the amount of returned coins for your coffee purchase.

## Requirements

- Python3.7
- Pipenv

## Installation

```sh
pipenv --threee
pipenv install
```

## Usage

```sh
pipenv run python src/return_coins.py <EUR_CENTS_INSERTED>
```

## Configurations

You can set the coffee price with `COFFEE_PRICE` environment variable. The default coffee price is `314` cents.
