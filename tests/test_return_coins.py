from src.return_coins import return_coins


def test_returns_correct_amount():
    inserted_amount = 2000
    valid_response = [
        (200, 8),
        (50, 1),
        (20, 1),
        (10, 1),
        (5, 1)
    ]

    returned_coins = return_coins(314, inserted_amount)
    assert type(returned_coins) is list
    assert returned_coins == valid_response
